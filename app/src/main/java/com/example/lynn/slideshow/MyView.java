package com.example.lynn.slideshow;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.LinearLayout;

import static com.example.lynn.slideshow.MainActivity.*;

/**
 * Created by lynn on 4/1/2017.
 */

public class MyView extends LinearLayout {

    public MyView(Context context) {
        super(context);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);



        int[] temp = {R.drawable.city1,R.drawable.city2,R.drawable.city3,R.drawable.city4,R.drawable.city5,
                      R.drawable.city6,R.drawable.city7,R.drawable.city8,R.drawable.city9,R.drawable.city10,
                      R.drawable.city11,R.drawable.city12,R.drawable.city13};

        drawables = new Drawable[temp.length];

        for (int counter=0;counter<drawables.length;counter++)
            drawables[counter] = ContextCompat.getDrawable(context,temp[counter]);

        view = new ImageView(context);

        Drawable drawable = ContextCompat.getDrawable(context,R.drawable.cow);

        view.setImageDrawable(drawables[index]);

        addView(view);

        view.setLayoutParams(layoutParams);

        view.setOnTouchListener(listener);
    }

}
