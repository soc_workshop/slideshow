package com.example.lynn.slideshow;

import android.view.MotionEvent;
import android.view.View;

import static com.example.lynn.slideshow.MainActivity.*;

/**
 * Created by lynn on 4/1/2017.
 */

public class MyListener implements View.OnTouchListener {
    private float start;

    @Override
    public boolean onTouch(View view1, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
           start = motionEvent.getX();
        else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            float end = motionEvent.getX();

            if (start > end)
                index--;
            else if (start < end)
                index++;

            if (index < 0)
                index = drawables.length-1;
            else if (index > drawables.length-1)
                index = 0;

            view.setImageDrawable(drawables[index]);
        }

        return true;
    }

}
